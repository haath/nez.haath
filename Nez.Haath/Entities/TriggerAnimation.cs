﻿
using Nez;
using Nez.Sprites;
using System;

using Nez.Textures;

using Nez.Haath.Extensions;

namespace Nez.Haath
{
	/// <summary>
	/// Plays an animation and provides feedback on what its sprite collided with on the first frame.
	/// </summary>
	public class TriggerAnimation : Entity
	{
		Sprite sprite;
		Collider collider;

		/// <summary>
		/// Will fire once for every trigger collider that was hit when the animation was added to the scene.
		/// </summary>
		public event Action<Collider> OnTrigger;

		/// <summary>
		/// Initialize a static sprite that will play for some duration and then destroy itself.
		/// </summary>
		/// <param name="sprite"></param>
		/// <param name="seconds"></param>
		public TriggerAnimation(Sprite sprite, ColliderType colliderType = ColliderType.Box, float seconds = 0.5f)
		{
			Core.schedule(seconds, t => Destroy());

			addComponent(sprite);
			AddCollider(colliderType, sprite);

			this.sprite = sprite;
		}

		/// <summary>
		/// Initialize an animation that will play and then destroy itself.
		/// </summary>
		/// <param name="animation">The animation to play.</param>
		public TriggerAnimation(SpriteAnimation animation, ColliderType colliderType = ColliderType.Box)
		{
			Sprite<int> sprite = addComponent(new Sprite<int>(animation.frames[0]));
			sprite.renderLayer = (int)RenderLayer.AboveCharacters;
			sprite.addAnimation(0, animation);
			sprite.play(0);
			sprite.onAnimationCompletedEvent += i => Destroy();

			AddCollider(colliderType, sprite);

			this.sprite = sprite;
		}

		void AddCollider(ColliderType colliderType, Sprite sprite)
		{
			switch (colliderType)
			{
				case ColliderType.Box:
					collider = addComponent(new BoxCollider(sprite.width, sprite.height));
					break;

				case ColliderType.Circle:
					collider = addComponent(new CircleCollider(sprite.width / 2));
					break;

				default:
					goto case ColliderType.Box;
			}

			collider.collidesWithLayers = (int)CollisionLayers.Characters;
			collider.physicsLayer = (int)CollisionLayers.None;
			collider.isTrigger = true;
		}

		public override void onAddedToScene()
		{
			foreach (CollisionResult res in collider.TriggersWith())
			{
				if (res.collider != null)
					OnTrigger?.Invoke(res.collider);
			}
		}

		public void FlipX()
		{
			sprite.flipX = true;
		}

		public void FlipY()
		{
			sprite.flipY = true;
		}

		void Destroy()
		{
			removeComponent(sprite);
			setEnabled(false);
		}
	}
}
