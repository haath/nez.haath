﻿using Nez;
using Nez.Tiled;
using Nez.Sprites;
using Nez.Textures;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Nez.Haath
{

	public class RmCharacter : Entity
	{
		const float DAMAGE_ANIMATION_SECONDS = 0.28f;

		protected Collider mapCollider { get; private set; }
		protected Collider characterCollider { get; private set; }

		protected Vector2 AnimationVelocity
		{
			get { return getComponent<RmSpriteAnimator>().Velocity; }
			set { getComponent<RmSpriteAnimator>().Velocity = value; }
		}

		public Vector2 FacingDirection
		{
			get
			{
				return getComponent<RmSpriteAnimator>().Direction;
			}
		}

		public RmCharacter(Texture2D sprite, bool tallSprite = true, string name = null) : base(name)
		{
			if (tallSprite)
			{
				addComponent(new RmTallSpriteAnimator(sprite));
			}
			else
			{
				addComponent(new RmShortSpriteAnimator(sprite));
			}

			// Set up the collider with the tiled map
			mapCollider = addComponent(new CircleCollider(16));
			mapCollider.collidesWithLayers = (int)(CollisionLayers.Ground | CollisionLayers.Characters | CollisionLayers.Walls);
			mapCollider.physicsLayer = (int)CollisionLayers.Characters;

			// Set up the collider to detect projectiles
			characterCollider = addComponent(new BoxCollider(-16, -44, 34, 58));
			characterCollider.collidesWithLayers = (int)CollisionLayers.Projectiles;
			characterCollider.physicsLayer = (int)CollisionLayers.Characters;
			characterCollider.isTrigger = true;
		}

		public override void update()
		{
			base.update();

			CharacterMover mover = getComponent<CharacterMover>();

			if (mover != null)
			{
				AnimationVelocity = mover.Velocity;
			}
		}
	}
}
