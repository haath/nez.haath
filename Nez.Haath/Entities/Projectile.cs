﻿using System;

using Nez;
using Nez.Sprites;

using Microsoft.Xna.Framework;

namespace Nez.Haath
{
	/// <summary>
	/// Entity wrapper for defining projectiles.
	/// </summary>
	public class Projectile : Entity
	{
		ProjectileMover mover;

		/// <summary>
		/// The velocity vector of the projectile.
		/// </summary>
		public Vector2 Velocity;

		/// <summary>
		/// The distance from its instantiation point at which the projectile will destory itself.
		/// </summary>
		public int MaxTravelDistance = int.MaxValue;

		/// <summary>
		/// Whether or not this projectle should destroy itself on impact. 
		/// <para>Default is true</para>
		/// </summary>
		public bool DestroyOnCollision = true;

		/// <summary>
		/// The gravity acceleration for this projectile, measuring in pixels/s/s
		/// </summary>
		public float Gravity = 0f;

		/// <summary>
		/// This event will fire when the projectile enters another trigger.
		/// </summary>
		public event Action<Collider> OnHit;

		Vector2 startingPosition;

		#region Constructors

		protected Projectile(ColliderType colliderType)
		{
			mover = addComponent<ProjectileMover>();

			Collider collider;

			switch (colliderType)
			{
				case ColliderType.Box:
					collider = addComponent<BoxCollider>();
					break;

				case ColliderType.Circle:
					collider = addComponent<CircleCollider>();
					break;

				default:
					goto case ColliderType.Box;
			}

			collider.collidesWithLayers = (int)CollisionLayers.Walls;
			collider.physicsLayer = (int)CollisionLayers.Projectiles;

			addComponent<TriggerDetector>().OnEnter += (other, self) => OnColliderEnter(other);
		}

		/// <summary>
		/// Initialize a projectile with a static sprite and set it in motion.
		/// </summary>
		/// <param name="sprite"></param>
		/// <param name="maxTravelDistance"></param>
		public Projectile(Sprite sprite, ColliderType colliderType = ColliderType.Box, int maxTravelDistance = int.MaxValue) 
			: this(colliderType)
		{
			addComponent(sprite);
			sprite.renderLayer = (int)RenderLayer.Projectiles;
		}


		/// <summary>
		/// Initialize an animated projectile and set it in motion.
		/// </summary>
		/// <param name="animation"></param>
		/// <param name="maxTravelDistance"></param>
		public Projectile(SpriteAnimation animation, ColliderType colliderType = ColliderType.Box, int maxTravelDistance = int.MaxValue) 
			: this(colliderType)
		{
			Sprite<int> sprite = addComponent(new Sprite<int>(animation.frames[0]));
			sprite.addAnimation(0, animation);
			sprite.play(0);
			sprite.renderLayer = (int)RenderLayer.Projectiles;
		}

		#endregion

		public override void onAddedToScene()
		{
			startingPosition = transform.position;
		}

		#region Overrides

		/// <summary>
		/// Override this method to get informed when the projectile enters a trigger on the Projectiles CollisionLayer.
		/// </summary>
		/// <param name="other">The collider the projectile collided with.</param>
		protected virtual void OnColliderEnter(Collider other)
		{
			OnHit?.Invoke(other);
		}

		public override void update()
		{
			base.update();

			Velocity.Y += Gravity * Time.deltaTime;

			if (mover.move(Velocity * Time.deltaTime) && DestroyOnCollision)
			{
				Destroy();
			}

			if ((transform.position - startingPosition).Length() > MaxTravelDistance)
			{
				Destroy();
			}
		}

		#endregion

		public void Destroy()
		{
			setEnabled(false);
		}
	}
}
