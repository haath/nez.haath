﻿

using Nez;
using Nez.UI;

using Microsoft.Xna.Framework;

namespace Nez.Haath
{
	public class Gui : Entity
	{
		UICanvas canvas;

		Element currentlyDragging = null;

		public Gui(string name = null) : base(name)
		{
			canvas = addComponent<UICanvas>();
			canvas.isFullScreen = true;
			canvas.renderLayer = (int)RenderLayer.Interface;
		}

		public T addElement<T>(T element) where T : Element
		{
			return canvas.stage.addElement<T>(element);
		}

		public override void update()
		{
			base.update();

			if (Input.leftMouseButtonPressed)
			{
				Element hit = canvas.stage.hit(Input.mousePosition);

				if (hit != null && hit is IDraggable)
				{
					Vector2 relative = hit.screenToLocalCoordinates(Input.mousePosition);

					if ((hit as IDraggable).TryGrab(relative))
					{
						Element elemToBringToFront = hit;
						while (elemToBringToFront != null)
						{
							elemToBringToFront.toFront();
							elemToBringToFront = elemToBringToFront.getParent();
						}

						(hit as IDraggable).OnGrab();
						
						currentlyDragging = hit;
					}
				}
			}

			if (Input.leftMouseButtonDown && currentlyDragging != null)
			{
				Vector2 movePos = currentlyDragging.screenToLocalCoordinates(Input.mousePosition);
				
				movePos = currentlyDragging.localToParentCoordinates(movePos);
				movePos -= new Vector2(
					currentlyDragging.getScaleX() * currentlyDragging.getWidth() / 2,
					currentlyDragging.getScaleY() * currentlyDragging.getHeight() / 2
					);

				currentlyDragging.setPosition(movePos.X, movePos.Y);

				(currentlyDragging as IDraggable).OnMove(Input.mousePositionDelta);
			}
			else if (currentlyDragging != null)
			{
				(currentlyDragging as IDraggable).OnRelease();
				currentlyDragging = null;
			}
		}
	}
}
