﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Nez;
using Nez.Textures;

namespace Nez.Haath
{
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// A basic Scene wrapper that allows for free sizing of the scene design without affecting the UI.
	/// <para>Also excludes the UI from any batcher jobs.</para>
	/// </summary>
	public class BaseScene : Scene, IFinalRenderDelegate
	{
		RenderLayerRenderer lightRenderer;
		ScreenSpaceRenderer screenSpaceRenderer;
		PolyLightPostProcessor lightProcessor;

		RenderLayerExcludeRenderer mainRenderer;

		public static event Action<object> OnLog;

		public BaseScene(int designWidth, int designHeight)
		{
			setDesignResolution(designWidth, designHeight, SceneResolutionPolicy.None);
			
			screenSpaceRenderer = new ScreenSpaceRenderer(100, (int)RenderLayer.Interface);
			screenSpaceRenderer.shouldDebugRender = false;

			finalRenderDelegate = this;

			lightRenderer = addRenderer(new RenderLayerRenderer(-1, (int)RenderLayer.Light));
			lightRenderer.renderTargetClearColor = new Color(10, 10, 10, 200);
			lightRenderer.renderTexture = new RenderTexture();

			lightProcessor = addPostProcessor(new PolyLightPostProcessor(0, lightRenderer.renderTexture))
				.setEnableBlur(false)
				.setBlurAmount(0f);
			lightProcessor.enabled = false;

			mainRenderer = addRenderer(new RenderLayerExcludeRenderer(0, (int)RenderLayer.Interface, (int)RenderLayer.Light));
		}

		public override void update()
		{
			base.update();
		}

		public override void initialize()
		{
			
		}

		/// <summary>
		/// Set the light level of the entire scene.
		/// </summary>
		/// <param name="lightLevel">The light level between 0.0f and 1.0f</param>
		public void SetLightLevel(float lightLevel)
		{
			if (lightLevel < 0f || lightLevel > 1f)
				throw new ArgumentException("Light level must be between 0.0f and 1.0f. Value given is: " + lightLevel);

			int alpha = (int)Math.Ceiling(lightLevel * 255);
			lightRenderer.renderTargetClearColor = new Color(10, 10, 10, alpha);
		}

		/// <summary>
		/// Enable or disable the post-processor that handles light rendering.
		/// </summary>
		/// <param name="isEnabled"></param>
		/// <returns></returns>
		public PolyLightPostProcessor SetLightProcessor(bool isEnabled)
		{
			lightProcessor.enabled = isEnabled;
			return lightProcessor;
		}

		#region IFinalRenderDelegate
		public Scene scene { get; set; }

		public void handleFinalRender(Color letterboxColor, RenderTarget2D source, Rectangle finalRenderDestinationRect, SamplerState samplerState)
		{
			Core.graphicsDevice.SetRenderTarget(null);
			Core.graphicsDevice.Clear(letterboxColor);
			Graphics.instance.batcher.begin(BlendState.Opaque, samplerState, DepthStencilState.None, RasterizerState.CullNone, null);
			Graphics.instance.batcher.draw(source, finalRenderDestinationRect, Color.White);
			Graphics.instance.batcher.end();

			screenSpaceRenderer.render(scene);
		}

		public void onAddedToScene() { }

		public void onSceneBackBufferSizeChanged(int newWidth, int newHeight)
		{
			screenSpaceRenderer.onSceneBackBufferSizeChanged(newWidth, newHeight);
		}
		#endregion
	}
}
