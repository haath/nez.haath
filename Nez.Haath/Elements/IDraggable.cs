﻿
using Microsoft.Xna.Framework;

namespace Nez.Haath
{
	/// <summary>
	/// Implement this interface to make an Element draggable either by mouse or touch.
	/// <para>Requires usage of the Engine.Gui class.</para>
	/// </summary>
	public interface IDraggable
	{
		/// <summary>
		/// This method should return true if the element should start getting dragged at the given point
		/// </summary>
		/// <param name="relativePoint"></param>
		/// <returns></returns>
		bool TryGrab(Vector2 relativePoint);

		/// <summary>
		/// Called when the element starts getting dragged.
		/// </summary>
		void OnGrab();

		/// <summary>
		/// Called when the element stops getting dragged
		/// </summary>
		void OnRelease();

		/// <summary>
		/// Called when this element has been moved by dragging.
		/// <para>The movement has already been applied to the position of the element when this mehtod is called.</para>
		/// </summary>
		/// <param name="delta">The distance that this element was just moved for</param>
		void OnMove(Point delta);
	}
}
