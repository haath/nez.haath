﻿using Nez;
using Nez.UI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Nez.Haath
{
	public class UItext : Element
	{
		IFont font;
		string text;
		float scale;

		public UItext(IFont font, string text, float scale)
		{
			this.font = font;
			this.text = text;
			this.scale = scale;
		}

		public UItext(IFont font, string text) : this(font, text, 1) { }

		public override void draw(Graphics graphics, float parentAlpha)
		{
			base.draw(graphics, parentAlpha);

			float x = getRight() - getWidth();
			float y = getBottom() - getHeight();

			graphics.batcher.drawString(font, text, new Vector2(x, y), Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);
		}
	}
}
