﻿
using Nez;
using Microsoft.Xna.Framework;

using Nez.Haath.Extensions;

namespace Nez.Haath
{
	using System;
	using System.Linq;
	using System.Threading.Tasks;
	using System.Collections.Generic;

	/// <summary>
	/// Mover that will update itself in order to reach a target according to the resuls of an IPathfinder
	/// </summary>
	public class PathfinderMover : CharacterMover, IUpdatable
	{
		// The amount the targeted entity can move before we re-calculate the pathfinding.
		const int TARGET_ENTITY_DISTANCE_UPDATE = 64;

		// How close to a point is close enough.
		const int TARGET_MARGIN					= 2;	

		/// <summary>
		/// The movement speed the entity will move with.
		/// </summary>
		public int MoveSpeed = 100;

		/// <summary>
		/// If true, the pathfinding will try to skip parts of the path whenever there is a clear path to the target. 
		/// Useful in games with 360-degree movement because the pathfinding algorithm works in a grid and assumes 4-way movement.
		/// <para>Default: true</para>
		/// </summary>
		public bool Improvise = true;

		/// <summary>
		/// The collision result of the last frame.
		/// </summary>
		public CollisionResult CollisionResult { get; private set; }

		/// <summary>
		/// Used to set the collider that this entity uses to move around the map. Useful together with the Improvise flag to able to get over corners.
		/// </summary>
		public Collider MovementCollider = null;

		IPathfinder pathFinder;

		List<Vector2> path;
		int pathTargetIndex;

		Entity targetEntity = null;
		Vector2 lastTargetEntityPosition;
		int targetGoalRange = 0;
		int targetMaxRange = int.MaxValue;

		public PathfinderMover(IPathfinder pathFinder, int moveSpeed = 100)
		{
			this.pathFinder = pathFinder;
			MoveSpeed = moveSpeed;
		}

		/// <summary>
		/// Reset the current target and set a path to the given position.
		/// </summary>
		/// <param name="position">The world coordinates to move to.</param>
		public void MoveTo(Vector2 position)
		{
			try
			{
				path = pathFinder.Pathfind(transform.position, position);
				pathTargetIndex = 0;
			}
			catch (Exception ex)
			{

			}
		}

		/// <summary>
		/// Follow a moving entity.
		/// </summary>
		/// <param name="entity">The entity to follow.</param>
		/// <param name="targetGoalRange">
		///		The range from the entity that we want to reach. 
		///		This is useful because sometimes  you don't want to collide with the entity you are trying to follow.
		/// </param>
		/// <param name="targetMaxRange">
		///		The maximum "vision" range in which to attempt to follow the entity. 
		///		If the entity is further no attemps will be made to pathfind to it.
		/// </param>
		public void Follow(Entity entity, int targetGoalRange = 0, int targetMaxRange = int.MaxValue)
		{
			targetEntity = entity;
			lastTargetEntityPosition = entity.position;
			this.targetGoalRange = targetGoalRange;
			this.targetMaxRange = targetMaxRange;

			if (DistanceToFinalTarget() < targetMaxRange)
				MoveTo(entity.position);
		}

		public void Stop()
		{
			path = null;
			pathTargetIndex = -1;
			Velocity = Vector2.Zero;
		}

		void IUpdatable.update()
		{
			UpdateVelocity();

			CollisionResult collisionResult;
			move(Velocity * Time.deltaTime, out collisionResult);

			// Update the public field.
			CollisionResult = collisionResult;
		}

		void UpdateVelocity()
		{
			float distanceToFinalTarget = DistanceToFinalTarget();

			if (distanceToFinalTarget > -1 && distanceToFinalTarget < targetGoalRange)
			{
				// We are within goal-range of the final target so we don't need to move any more.
				Velocity = Vector2.Zero;
				return;
			}

			if (distanceToFinalTarget > -1 && distanceToFinalTarget > targetMaxRange && targetEntity != null)
			{
				// We are following an entity and are currently too far from it.
				Velocity = Vector2.Zero;
				return;
			}

			if (targetEntity != null)
			{
				// We are currently following an entity.
				UpdatePathfinding();
			}

			if (path != null && pathTargetIndex >= 0 && pathTargetIndex < path.Count)
			{
				Vector2 target = path[pathTargetIndex];

				if (WithinMarginOf(target))
				{
					// We reached our current target, get the next one.
					target = NextTarget();
				}

				// Get the angle between the velocity we just moved with and the new distance to the target
				double angle = Velocity.AngleTo(target - transform.position);
				if (Math.Abs(angle) > Math.PI)
				{
					// We overshot our previous target, move to the next one.
					target = NextTarget();
				}

				if (pathTargetIndex > -1)
				{
					// If we did not yet reach our destination, set the velocity.
					Velocity = MoveSpeed * (target - transform.position).Normalized();
				}
				else
				{
					// We reached, stop moving.
					Velocity = Vector2.Zero;
				}
			}
		}

		void UpdatePathfinding()
		{
			float distanceFromKnownPosition = (targetEntity.position - lastTargetEntityPosition).Length();

			if (distanceFromKnownPosition > TARGET_ENTITY_DISTANCE_UPDATE)
			{
				// Entity moved too far from its last known position, rec-calculate the pathfinding
				Follow(targetEntity, targetGoalRange);
			}
		}

		Vector2 NextTarget()
		{
			if (pathTargetIndex == path.Count - 1)
			{
				// We reached the end of the path
				pathTargetIndex = -1;
				return Vector2.Zero;
			}

			if (!Improvise)
			{
				// We are strictly following the grid
				// Return the next target
				return path[++pathTargetIndex];
			}

			for (int i = pathTargetIndex + 1; i < path.Count; i++)
			{
				Vector2 target = path[i];

				if (ClearPathToTarget(target))
				{
					// There is a clear path to this target.
					// Keep going down the chain.
				}
				else if (i == pathTargetIndex + 1)
				{
					// If the first part of the path is not visible then the obstacle is not part of the map and it's probably some other entity.
					// Attempt to move in that direction regardless.
					pathTargetIndex = i;
					return path[pathTargetIndex];
				}
				else
				{
					// There is not a clear path to this target.
					// Break the loop and return the previous target.
					pathTargetIndex = i - 1;
					return path[pathTargetIndex];
				}
			}

			// If we finished the above loop it means we reached the end with a clear path.
			// Go straight for the final target.
			pathTargetIndex = path.Count - 1;
			return path[pathTargetIndex];
		}
		
		bool ClearPathToTarget(Vector2 target)
		{
			// Check to see if our center has a path to the target.
			if (Physics.linecast(transform.position, target).collider != null)
			{
				return false;
			}

			if (MovementCollider != null)
			{
				// If we have a collider we need to also check some of our collider's edges to make sure we can get over corners.
				Vector2 c = MovementCollider.bounds.center;

				Vector2 top = new Vector2(c.X, MovementCollider.bounds.top + 1);
				Vector2 bot = new Vector2(c.X, MovementCollider.bounds.bottom - 1);
				Vector2 left = new Vector2(MovementCollider.bounds.left + 1, c.Y);
				Vector2 right = new Vector2(MovementCollider.bounds.right - 1, c.Y);

				Vector2[] points = new Vector2[] { top, bot, left, right };

				for (int i = 0; i < points.Length; i++)
				{
					RaycastHit obstacle = Physics.linecast(points[i], target);

					if (obstacle.collider != null && obstacle.collider != MovementCollider)
					{
						Debug.drawLine(points[i], target, Color.Red, 0.5f);
						Debug.drawPixel(obstacle.point, 6, Color.Purple, 0.5f);
						return false;
					}
				}
			}

			// We didn't find any colliders in any of the linecasts
			return true;
		}

		bool WithinMarginOf(Vector2 position)
		{
			return Math.Abs(transform.position.X - position.X) < TARGET_MARGIN
				&& Math.Abs(transform.position.Y - position.Y) < TARGET_MARGIN;
		}

		float DistanceToFinalTarget()
		{
			if (path == null || path.Count == 0)
				return -1f;

			Vector2 target = targetEntity == null ? path.Last() : targetEntity.position;

			return (target - transform.position).Length();
		}
		
		public override void debugRender(Graphics graphics)
		{
			for (int i = 0; path != null && i >= 0 && i < path.Count; i++)
			{
				Vector2 p = path[i];

				Color color = i > pathTargetIndex ? Color.Red : Color.Yellow;

				if (i == pathTargetIndex)
					color = Color.GreenYellow;

				Debug.drawPixel(p.X, p.Y, 6, color);
			}
		}
	}
}
