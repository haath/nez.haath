﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Nez.Haath
{
	using Extensions;

	public class TopDownPlayerController : CharacterMover, IUpdatable
	{
		VirtualIntegerAxis xAxis;
		VirtualIntegerAxis yAxis;
		VirtualIntegerAxis xLookAxis;
		VirtualIntegerAxis yLookAxis;
		VirtualButton interact;
		VirtualButton attack;
		VirtualButton cancel;
		VirtualButton[] hotbar;

		public event Action<Vector2> OnLook;
		public event Action OnInteract;
		public event Action<Interactable> OnInteractWith;
		public event Action OnAttack;
		public event Action OnCancel;
		public event Action<int> OnHotbar;

		public float GamePadDeadzone = 0.1f;
		public float AttackInterval = 0f;
		public int MovementSpeed = 100;
		public Func<Vector2> FacingDirection;

		public TopDownPlayerController()
		{
			// Movement
			xAxis = new VirtualIntegerAxis();
			xAxis.nodes.Add(new VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.A, Keys.D));
			xAxis.nodes.Add(new VirtualAxis.GamePadLeftStickX(deadzone: GamePadDeadzone));
			yAxis = new VirtualIntegerAxis();
			yAxis.nodes.Add(new VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.W, Keys.S));
			xAxis.nodes.Add(new VirtualAxis.GamePadLeftStickY(deadzone: GamePadDeadzone));

			// Firing
			xLookAxis = new VirtualIntegerAxis();
			xLookAxis.nodes.Add(new VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.Left, Keys.Right));
			xLookAxis.nodes.Add(new VirtualAxis.GamePadRightStickX(deadzone: GamePadDeadzone));
			yLookAxis = new VirtualIntegerAxis();
			yLookAxis.nodes.Add(new VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehavior.TakeNewer, Keys.Up, Keys.Down));
			yLookAxis.nodes.Add(new VirtualAxis.GamePadRightStickY(deadzone: GamePadDeadzone));

			// Interaction
			interact = new VirtualButton();
			interact.nodes.Add(new VirtualButton.KeyboardKey(Keys.E));

			// Attack
			attack = new VirtualButton();
			attack.nodes.Add(new VirtualButton.KeyboardKey(Keys.F));

			// Escape
			cancel = new VirtualButton();
			cancel.nodes.Add(new VirtualButton.KeyboardKey(Keys.Escape));

			// Hotbar
			hotbar = new VirtualButton[10];
			for (int i = 0; i < 10; i++)
			{
				Keys key = (Keys)(48 + ((i + 1) % 10));
				hotbar[i] = new VirtualButton();
				hotbar[i].nodes.Add(new VirtualButton.KeyboardKey(key));
			}
		}

		public override void onAddedToEntity()
		{
			// Add other components
			entity.addComponent(new Interactable(-25, -25, 50, 50));
			entity.addComponent(new FollowCamera(entity, FollowCamera.CameraStyle.LockOn));
			entity.addComponent<Mover>();
		}

		void IUpdatable.update()
		{
			/*
			 * Interaction
			 */
			if (interact.isPressed)
			{
				entity.getComponent<RmSpriteAnimator>()?
					  .InteractAnimation();

				OnInteract?.Invoke();

				Interactable interactive;
				if (TryInteract(out interactive))
				{
					OnInteractWith?.Invoke(interactive);
				}
			}

			/*
			 * Looking
			 */
			Vector2 lookDir = new Vector2(xLookAxis.value, yLookAxis.value);
			if (lookDir != Vector2.Zero)
			{
				OnLook?.Invoke(lookDir);
			}

			/*
			 * Firing
			 */
			if (attack.isDown && Time.checkEvery(AttackInterval))
			{
				OnAttack?.Invoke();
			}

			/*
			 * Cancel
			 */
			if (cancel.isPressed)
			{
				OnCancel?.Invoke();
			}

			/*
			 * Hotbar
			 */
			for (int i = 0; i < 10; i++)
			{
				if (hotbar[i].isPressed)
				{
					OnHotbar?.Invoke(i);
				}
			}

			/*
			 * Movement
			 */
			Vector2 dir = new Vector2(xAxis.value, yAxis.value);

			Velocity = MovementSpeed * dir.Normalized();

			CollisionResult res;
			entity.getComponent<Mover>().move(Velocity * Time.deltaTime, out res);
		}

		bool TryInteract(out Interactable other)
		{
			bool interacted = false;
			other = null;

			if (FacingDirection == null)
				return false;

			Rectangle ownRect = entity.getComponent<Interactable>().InteractRect();
			Ray ray = new Ray(transform.position.toVector3(), FacingDirection().toVector3());

			List<Interactable> interactives = Core.scene.findComponentsOfType<Interactable>();

			foreach (Interactable interactive in interactives)
			{
				if (interactive.entity != entity && ownRect.Intersects(interactive.InteractRect()))
				{
					// Something interactive is inside our interactive rectangle, check to see if we are also facing towards to
					float? dist = interactive.InteractRect().rayIntersects(ray);

					if (dist != null)
					{
						// We interacted with something

						interactive.Interact(entity);

						other = interactive;
						interacted = true;
					}
				}
			}

			return interacted;
		}
	}
}
