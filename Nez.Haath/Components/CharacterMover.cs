﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

namespace Nez.Haath
{
	public abstract class CharacterMover : Mover
	{
		/// <summary>
		/// The velocity of this mover as of the last frame.
		/// </summary>
		public Vector2 Velocity { get; protected set; }
	}
}
