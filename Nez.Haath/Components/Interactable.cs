﻿using System;

using Nez;
using Nez.PhysicsShapes;

using Microsoft.Xna.Framework;

namespace Nez.Haath
{
	/// <summary>
	/// Lets the entity it is attached to be interacted with.
	/// </summary>
	public class Interactable : Component
	{
		int offsetX;
		int offsetY;
		int width;
		int height;

		/// <summary>
		/// This event will fire on interaction. The parameter will be the entity that interacted with this once.
		/// </summary>
		public event Action<Entity> OnInteract;

		/// <summary>
		/// Initialize a new interactive area around the center of this entity.
		/// </summary>
		/// <param name="offsetX">Local X offset from the center of the entity.</param>
		/// <param name="offsetY">Local Y offset from the center of the entity.</param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		public Interactable(int offsetX, int offsetY, int width, int height)
		{
			this.offsetX = offsetX;
			this.offsetY = offsetY;
			this.width = width;
			this.height = height;
		}

		/// <summary>
		/// Gets the rectangle area for this interactive.
		/// </summary>
		/// <returns></returns>
		public Rectangle InteractRect()
		{
			Rectangle rect = new Rectangle(transform.position.ToPoint(), new Point(width, height));
			rect.Offset(offsetX, offsetY);
			return rect;
		}

		public void Interact(Entity user)
		{
			OnInteract?.Invoke(user);
		}

		/// <summary>
		/// Draw the rectangle if debug rendering is enabled.
		/// </summary>
		/// <param name="graphics"></param>
		public override void debugRender(Graphics graphics)
		{
			graphics.batcher.drawHollowRect(InteractRect(), Color.Cyan, Debug.Size.lineSizeMultiplier);
		}
	}
}
