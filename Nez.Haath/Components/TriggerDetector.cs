﻿
using System;
using Nez;

namespace Nez.Haath
{
	public class TriggerDetector : Component, ITriggerListener
	{
		public event Action<Collider, Collider> OnEnter;
		public event Action<Collider, Collider> OnExit;

		#region ITriggerListener

		public void onTriggerEnter(Collider other, Collider self)
		{
			OnEnter?.Invoke(other, self);
		}

		public void onTriggerExit(Collider other, Collider self)
		{
			OnExit?.Invoke(other, self);
		}

		#endregion
	}
}
