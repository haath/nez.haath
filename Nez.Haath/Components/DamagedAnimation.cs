﻿
using Nez;
using Nez.Sprites;

using Microsoft.Xna.Framework;

namespace Nez.Haath
{
	/// <summary>
	/// Attach to an entity and then call DamagedAnimation.Play() to apply a color to each of its sprites for a given duration.
	/// <para></para>
	/// </summary>
	public class DamagedAnimation : Component
	{
		Color baseColor = Color.White;
		ITimer timer;

		public void Play(float seconds, Color color)
		{
			if (timer != null)
			{
				timer.reset();
				return;
			}

			foreach (Sprite s in entity.getComponents<Sprite>())
			{
				s.color = color;
			}

			timer = Core.schedule(seconds, t =>
			{
				timer = null;
				Restore();
			});
		}

		/// <summary>
		/// Applies Color.Red for the given duration.
		/// </summary>
		/// <param name="seconds"></param>
		public void Play(float seconds)
		{
			Play(seconds, Color.Red);
		}

		void Restore()
		{
			foreach (Sprite s in entity.getComponents<Sprite>())
			{
				s.color = baseColor;
			}
		}
	}
}
