﻿
using System;
using System.Collections.Generic;

using Nez;
using Nez.Sprites;
using Nez.Textures;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Nez.Haath
{
	public class RmTallSpriteAnimator : RmSpriteAnimator
	{
		Sprite<Animation> topAnimation;
		Sprite<Animation> botAnimation;

		public RmTallSpriteAnimator(Texture2D spriteSheet)
		{
			List<Subtexture> topSprites = new List<Subtexture>();
			List<Subtexture> botSprites = new List<Subtexture>();

			// Cut each sprite into two equally-sized parts
			foreach (Subtexture s in Subtexture.subtexturesFromAtlas(spriteSheet, 52, 72))
			{
				Rectangle top = s.sourceRect;
				top.Width = 34;
				top.Height = 32;
				Rectangle bot = top.clone();

				top.Offset(8, 8);
				bot.Offset(8, 40);

				topSprites.Add(new Subtexture(spriteSheet, top));
				botSprites.Add(new Subtexture(spriteSheet, bot));
			};

			// Set up the top part of the sprite to be rendered above other characters
			topAnimation = GetSprite(topSprites);
			topAnimation.renderLayer = (int)RenderLayer.Characters - 1;
			topAnimation.localOffset = new Vector2(0, -32);

			// Set up the bottom part of the sprite on the character layer
			botAnimation = GetSprite(botSprites);
			botAnimation.renderLayer = (int)RenderLayer.Characters;
		}

		public override void onAddedToEntity()
		{
			entity.addComponent(topAnimation);
			entity.addComponent(botAnimation);

			base.onAddedToEntity();
		}

		protected override Animation CurrentAnimation()
		{
			return topAnimation.currentAnimation;
		}

		protected override void PlayAnimation(Animation animation, int frame = 0)
		{
			topAnimation.play(animation, frame);
			botAnimation.play(animation, frame);
		}

		protected override void StopAnimation()
		{
			topAnimation.stop();
			botAnimation.stop();
		}
	}
}
