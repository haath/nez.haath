﻿using System;
using System.Collections.Generic;

using Nez;
using Nez.Sprites;
using Nez.Textures;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Nez.Haath
{
	public class RmShortSpriteAnimator : RmSpriteAnimator
	{
		Sprite<Animation> animation;

		public RmShortSpriteAnimator(Texture2D spriteSheet)
		{
			List<Subtexture> subtextures = Subtexture.subtexturesFromAtlas(spriteSheet, 52, 72);

			animation = GetSprite(subtextures);
			animation.renderLayer = (int)RenderLayer.Characters;
		}

		protected override Animation CurrentAnimation()
		{
			return animation.currentAnimation;
		}

		protected override void PlayAnimation(Animation animation, int frame = 0)
		{
			this.animation.play(animation, frame);
		}

		protected override void StopAnimation()
		{
			animation.stop();
		}
	}
}
