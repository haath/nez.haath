﻿
using Nez;
using Nez.Sprites;
using Nez.Textures;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Nez.Haath
{
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// This class mostly servers the purpose of deadling with the issue in top-down games where you want a sprite to be behind another sprite if it is lower on the y-axis.
	/// <para>In this implementation, the sprite is split into two parts with one on each layer and then collision makes sure that they are never intersected by something.</para>
	/// </summary>
	public abstract class RmSpriteAnimator : Component, IUpdatable
	{

		Switch animationLock;
		Switch directionLock;

		public Vector2 Velocity;

		Vector2 direction;
		public Vector2 Direction
		{
			get { return direction; }
			set { if (!directionLock) direction = value; }
		}

		public RmSpriteAnimator()
		{
			animationLock = new Switch();
			directionLock = new Switch();

			Velocity = Vector2.Zero;
			Direction = new Vector2(0, 1);
		}

		public override void onAddedToEntity()
		{
			TurnTowards(Direction);
		}

		protected Sprite<Animation> GetSprite(List<Subtexture> spriteParts)
		{
			Sprite<Animation> sprite = new Sprite<Animation>(spriteParts[1]);
			sprite.addAnimation(Animation.WalkDown, GetAnimation(spriteParts, 0));
			sprite.addAnimation(Animation.WalkLeft, GetAnimation(spriteParts, 1));
			sprite.addAnimation(Animation.WalkRight, GetAnimation(spriteParts, 2));
			sprite.addAnimation(Animation.WalkUp, GetAnimation(spriteParts, 3));
			return sprite;
		}

		SpriteAnimation GetAnimation(List<Subtexture> spriteParts, int row)
		{
			row = row * 3;

			SpriteAnimation anim = new SpriteAnimation(new List<Subtexture>()
			{
				spriteParts[row + 0], spriteParts[row + 1], spriteParts[row + 2], spriteParts[row + 1]
			})
			{
				loop = true,
				fps = 6
			};

			return anim;
		}

		void IUpdatable.update()
		{
			if (!animationLock)
			{
				TurnTowards(Velocity);
			}
		}

		/// <summary>
		/// Lock the direction of the sprite for a given duration
		/// </summary>
		/// <param name="seconds"></param>
		/// <returns></returns>
		public RmSpriteAnimator LockDirectionFor(float seconds)
		{
			if (!directionLock)
				directionLock.FlipFor(seconds);
			return this;
		}

		/// <summary>
		/// Play the interact animation.
		/// </summary>
		/// <param name="seconds"></param>
		/// <returns></returns>
		public RmSpriteAnimator InteractAnimation(float seconds = 0.25f)
		{
			if (!animationLock)
			{
				PlayAnimation(CurrentAnimation(), 0);

				animationLock.FlipFor(seconds);
			}
			return this;
		}

		public RmSpriteAnimator TurnTowards(Vector2 direction)
		{
			Sprite<Animation> a = entity.getComponent<Sprite<Animation>>();
			Animation anim = Animation.None;

			if (Math.Abs(direction.X) >= Math.Abs(direction.Y) && Math.Abs(direction.X) > 0)
			{
				if (direction.X > 0)
				{
					anim = Animation.WalkRight;
					Direction = new Vector2(1, 0);
				}
				else
				{
					anim = Animation.WalkLeft;
					Direction = new Vector2(-1, 0);
				}
				Direction = direction;
			}
			else if (Math.Abs(direction.Y) > Math.Abs(direction.X) && Math.Abs(direction.Y) > 0)
			{
				if (direction.Y > 0)
				{
					anim = Animation.WalkDown;
					Direction = new Vector2(0, 1);
				}
				else
				{
					anim = Animation.WalkUp;
					Direction = new Vector2(0, -1);
				}
				Direction = direction;
			}
			else if (a.currentAnimation != Animation.None)
			{
				PlayAnimation(a.currentAnimation, 1);
				StopAnimation();
			}

			if (!a.isAnimationPlaying(anim) && anim != Animation.None && !directionLock)
			{
				PlayAnimation(anim);
			}
			return this;
		}

		protected abstract Animation CurrentAnimation();

		protected abstract void PlayAnimation(Animation animation, int frame = 0);

		protected abstract void StopAnimation();

		public enum Animation
		{
			None,
			WalkDown,
			WalkLeft,
			WalkRight,
			WalkUp
		}
	}
}
