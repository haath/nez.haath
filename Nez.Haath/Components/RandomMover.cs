﻿
using Nez;

using System.Diagnostics;

using Microsoft.Xna.Framework;

using Nez.Haath.Extensions;

namespace Nez.Haath
{
	/// <summary>
	/// Will move the entity randomly both in the X and Y axis.
	/// </summary>
	public class RandomMover : CharacterMover, IUpdatable
	{
		/// <summary>
		/// The frequency with which random movements will take place. A value of 1f means that the entity will move all the time and a value of 0f means that it will never move.
		/// <para>Default: 0.5f</para>
		/// </summary>
		public float MoveFrequency = 0.5f;

		/// <summary>
		/// The movement speed the entity will move with.
		/// </summary>
		public int MoveSpeed = 100;

		/// <summary>
		/// The duration in milliseconds of each inidividual move segment in the same direction.
		/// <para>Default: 500</para>
		/// </summary>
		public int MoveDuration = 500;
		
		Stopwatch movementTimer;

		/// <summary>
		/// The collision result of the last frame.
		/// </summary>
		public CollisionResult CollisionResult { get; private set; }

		public RandomMover(int moveSpeed)
		{
			MoveSpeed = moveSpeed;

			movementTimer = new Stopwatch();
		}

		void IUpdatable.update()
		{

			if (!movementTimer.IsRunning && (Random.chance(MoveFrequency * Time.deltaTime)))
			{
				Vector2 direction = Extensions.Extensions.NextDirection();

				Velocity = MoveSpeed * direction;
				movementTimer.Start();
			}
			else if (movementTimer.ElapsedMilliseconds > MoveDuration || CollisionResult.collider != null)
			{
				movementTimer.Stop();
				movementTimer.Reset();
				Velocity = Vector2.Zero;
			}

			CollisionResult collisionResult;
			move(Velocity * Time.deltaTime, out collisionResult);

			// Update the public field.
			CollisionResult = collisionResult;
		}
	}
}
