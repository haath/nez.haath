﻿using Nez;


namespace Nez.Haath.Map
{
	using System.Collections;
	using System.Collections.Generic;

	/// <summary>
	/// Holds the description of a Tiled map.
	/// <para>Can map layers to renderLayers and clasify which layers should pose collision.</para>
	/// </summary>
	public class TiledMapDescription : IEnumerable<TiledLayerDescription>
	{
		List<TiledLayerDescription> description = new List<TiledLayerDescription>();

		public void Add(string layerName, RenderLayer renderLayer, CollisionLayers collisionLayers = CollisionLayers.None, Material layerMaterial = null)
		{
			description.Add(new TiledLayerDescription(layerName, renderLayer, collisionLayers, layerMaterial));
		}


		#region IEnumerator
		public IEnumerator<TiledLayerDescription> GetEnumerator()
		{
			return description.GetEnumerator();
		}
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion
	}
}
