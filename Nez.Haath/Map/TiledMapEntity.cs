﻿using Nez;
using Nez.Tiled;
using Nez.AI.Pathfinding;

using Microsoft.Xna.Framework;

using Nez.Haath.Extensions;

namespace Nez.Haath.Map
{
	using System.Linq;
	using System.Collections.Generic;

	/// <summary>
	/// A complete TiledMap entity where each layer is a component
	/// </summary>
	public class TiledMapEntity : Entity, IPathfinder
	{
		TiledMap tiledMap;

		int mapOffsetX;
		int mapOffsetY;
		
		AstarGrid pathfindingGraph;

		List<TiledMapEntity> children = new List<TiledMapEntity>();

		/// <summary>
		/// Gets the underlying TiledMap that this entity was initialized on.
		/// </summary>
		public TiledMap TiledMap
		{
			get { return tiledMap; }
		}

		#region Constructors

		TiledMapEntity(TiledMap tiledMap, string name, int mapOffsetX, int mapOffsetY) : base(name)
		{
			this.tiledMap = tiledMap;
			this.mapOffsetX = mapOffsetX;
			this.mapOffsetY = mapOffsetY;
		}

		/// <summary>
		/// Initialize a TiledMap entity with no collision and all the layers visible on a single renderLayer
		/// </summary>
		/// <param name="tiledMap">The TiledMap resource</param>
		public TiledMapEntity(TiledMap tiledMap, int mapOffsetX = 0, int mapOffsetY = 0) : this(tiledMap, (string)null, mapOffsetX, mapOffsetY)
		{
			addComponent(new TiledMapComponent(tiledMap));
		}

		/// <summary>
		/// Initialize a custom TiledMap entity where each layer had been mapped to a renderLayer
		/// </summary>
		/// <param name="tiledMap">The TiledMap resource</param>
		/// <param name="description">The description of the layers of this map</param>
		/// <param name="name">This entity's name</param>
		public TiledMapEntity(TiledMap tiledMap, TiledMapDescription description, string name, int mapOffsetX = 0, int mapOffsetY = 0) 
			: this(tiledMap, name, mapOffsetX, mapOffsetY)
		{
			pathfindingGraph = new AstarGrid();
			
			foreach (TiledLayerDescription layer in description)
			{
				TiledMapComponent layerComponent;

				// Initialize component
				if (layer.CollisionLayers != CollisionLayers.None)
				{
					layerComponent = new TiledMapComponent(tiledMap, layer.LayerName);
					layerComponent.physicsLayer = (int)layer.CollisionLayers;

					addComponent(layerComponent);
					
					pathfindingGraph.AddLayer(tiledMap.getLayer<TiledTileLayer>(layer.LayerName));
				}
				else
					layerComponent = addComponent(new TiledMapComponent(tiledMap));

				// Set this layer to render if it is visible
				if (layer.RenderLayer != RenderLayer.Hidden)
					layerComponent.setLayerToRender(layer.LayerName);
				else
					layerComponent.setLayersToRender();

				// Set the render layer that this component is on
				layerComponent.renderLayer = (int)layer.RenderLayer;

				// If this layer also has a custom material, apply it
				if (layer.LayerMaterial != null)
					layerComponent.material = layer.LayerMaterial;
			}
		}

		public TiledMapEntity(TiledMap tiledMap, TiledMapDescription description, int mapOffsetX = 0, int mapOffsetY = 0) 
			: this(tiledMap, description, null, mapOffsetX, mapOffsetY) { }

		#endregion

		public void AppendMap(TiledMapEntity map, Point appendPosition)
		{
			children.Add(map);

			// Set the map's offset so that it appears in the desired position
			map.mapOffsetX = appendPosition.X * tiledMap.tileWidth;
			map.mapOffsetY = appendPosition.Y * tiledMap.tileHeight;

			// Append the map's pathfinding graph to ours
			foreach (Point otherMapPoint in map.pathfindingGraph.walls)
			{
				pathfindingGraph.AddWall(otherMapPoint + new Point(appendPosition.X, appendPosition.Y));
			}
		}

		public override void onAddedToScene()
		{
			// Add our children to the scene
			foreach (TiledMapEntity map in children)
			{
				scene.addEntity(map);
			}

			// Set our position
			transform.setLocalPosition(new Vector2(mapOffsetX, mapOffsetY));

			foreach (TiledMapEntity map in children)
			{
				map.setParent(this);
			}
		}

		#region IPathfinder

		/// <summary>
		/// Plot a path from the start to the end. The path's parts will point to exactly the center of each tile, except for the first and last part
		/// which will point to the given arguments.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		public List<Vector2> Pathfind(Vector2 start, Vector2 end)
		{
			Point startingPoint = WorldToTilePosition(start);
			Point endingPoint = WorldToTilePosition(end);

			List<Point> path = pathfindingGraph.search(startingPoint, endingPoint);
			
			List<Vector2> worldPositionPath = path?.Select(p => TileToWorldPosition(p)).ToList();

			if (worldPositionPath != null)
			{
				// Replace the start and the end of the of the path with the given coordinates instead of the map-centered ones.
				worldPositionPath.Insert(0, start);
				worldPositionPath.Add(end);
			}

			return worldPositionPath;
		}

		#endregion

		/// <summary>
		/// Given the grid coordinates of a tile, returns a Vector2 to the tile's centre.
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public Vector2 TileToWorldPosition(Point pos)
		{
			return new Vector2((pos.X + 0.5f) * tiledMap.tileWidth, (pos.Y + 0.5f) * tiledMap.tileHeight)
					+ transform.position;
		}

		/// <summary>
		/// Shortcut to TiledMap.worldToTilePosition that also takes the map's position into consideration.
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="clamp"></param>
		/// <returns></returns>
		public Point WorldToTilePosition(Vector2 pos, bool clamp = false)
		{
			return tiledMap.worldToTilePosition(pos - transform.position, clamp);
		}

		/// <summary>
		/// Returns true if the given position is within the bounds of this map.
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public bool ContainsWorldPosition(Vector2 pos)
		{
			pos = pos - transform.position;
			Point p = tiledMap.worldToTilePosition(pos, false);

			return p.X >= 0 && p.X < tiledMap.width
				&& p.Y >= 0 && p.Y < tiledMap.height;
		}

		/// <summary>
		/// Get an object of the map by name.
		/// <para>Can be part of any object group.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public TiledObject GetObjectWithName(string name)
		{
			foreach (TiledObjectGroup group in tiledMap.objectGroups)
			{
				TiledObject obj = group.objectWithName(name);
				if (obj != null)
				{
					TiledObject clone = obj.Clone();
					clone.position = transform.position + obj.position;
					return clone;
				}
			}

			foreach (TiledMapEntity child in children)
			{
				TiledObject obj = child.GetObjectWithName(name);
				if (obj != null)
					return obj;
			}
			return null;
		}

		/// <summary>
		/// Get all objects on a map that have a specific type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public List<TiledObject> GetObjectsWithType(string type)
		{
			List<TiledObject> objects = new List<TiledObject>();
			foreach (TiledObjectGroup group in tiledMap.objectGroups)
			{
				foreach (TiledObject obj in group.objects)
				{
					if (obj.type.Equals(type))
					{
						TiledObject clone = obj.Clone();
						clone.position = transform.position + obj.position;
						objects.Add(clone);
					}
				}
			}
			foreach (TiledMapEntity child in children)
			{
				objects.AddRange(child.GetObjectsWithType(type));
			}
			return objects;
		}

		
	}
}
