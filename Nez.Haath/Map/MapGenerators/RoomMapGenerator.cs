﻿
using System;
using System.Linq;
using System.Collections.Generic;

using Nez;
using Nez.Tiled;

using Microsoft.Xna.Framework;

namespace Nez.Haath.Map
{
	public class RoomMapGenerator : MapGenerator
	{
		public RoomMapGenerator(List<TiledMap> tiledMaps, TiledMapDescription mapDescription) : base(tiledMaps, mapDescription)
		{
		}

		public override TiledMapEntity Generate()
		{
			// Start on a random map
			mainEntity = RecursiveMapBuild(tiledMaps.randomItem(), EntryPoint.None);
			
			return mainEntity;
		}

		TiledMapEntity RecursiveMapBuild(TiledMap tiledMap, EntryPoint excludePoint)
		{
			TiledMapEntity mapEntity = new TiledMapEntity(tiledMap, mapDescription);

			IEnumerable<TiledObject> openEntries = mapEntity.GetObjectsWithType("entry")
				.Where(obj => EntryLocation(mapEntity.TiledMap, obj) != excludePoint);

			// We want to fill all of our open entry points
			foreach (TiledObject entry in openEntries)
			{
				EntryPoint entryPoint = EntryLocation(tiledMap, entry);

				TiledObject otherMapEntryPoint;
				TiledMap mapToAppend = GetMapToAttachEntryPoint(entryPoint, out otherMapEntryPoint);
				TiledMapEntity mapEntityToappend = RecursiveMapBuild(mapToAppend, (EntryPoint)(-(int)entryPoint));

				// Get the tile coordinates just outside the entry point
				Point entryAttachPoint = mapEntity.WorldToTilePosition(entry.position);
				entryAttachPoint = ToOutsideEntryPoint(entryAttachPoint, entryPoint);

				// Get the offset of the other map's entry point
				Point otherMapEntryOffset = mapEntityToappend.WorldToTilePosition(otherMapEntryPoint.position);

				mapEntity.AppendMap(mapEntityToappend, entryAttachPoint - otherMapEntryOffset);
			}

			return mapEntity;
		}

		TiledMap GetMapToAttachEntryPoint(EntryPoint entryPoint, out TiledObject otherMapEntryPoint)
		{
			// We need maps that contain the opposite entry point
			// F.e if we have an open entry to the north, we need a map that has one to the south
			EntryPoint targetPoint = (EntryPoint)(-(int)entryPoint);
			otherMapEntryPoint = null;

			Dictionary<TiledMap, TiledObject> options = new Dictionary<TiledMap, TiledObject>();

			foreach (TiledMap map in tiledMaps)
			{
				foreach (TiledObjectGroup group in map.objectGroups)
				{
					TiledObject entry = group.objects.FirstOrDefault(obj => EntryLocation(map, obj) == targetPoint);

					if (entry != null && !options.ContainsKey(map))
					{
						options.Add(map, entry);
					}
				}
			}

			if (options.Count == 0)
				return null;

			TiledMap mapPick = options.Keys.ToList().randomItem();
			otherMapEntryPoint = options[mapPick];
			return mapPick;
		}

		Point ToOutsideEntryPoint(Point point, EntryPoint location)
		{
			switch (location)
			{
				case EntryPoint.North:
					point.Y -= 1;
					break;

				case EntryPoint.South:
					point.Y += 1;
					break;

				case EntryPoint.East:
					point.X += 1;
					break;

				case EntryPoint.West:
					point.X -= 1;
					break;
			}

			return point;
		}

		EntryPoint EntryLocation(TiledMap tiledMap, TiledObject entry)
		{
			Point entryPoint = tiledMap.worldToTilePosition(entry.position);

			if (entryPoint.Y == 0)
				return EntryPoint.North;
			else if (entryPoint.Y == tiledMap.height - 1)
				return EntryPoint.South;
			else if (entryPoint.X == 0)
				return EntryPoint.West;
			else if (entryPoint.X == tiledMap.width - 1)
				return EntryPoint.East;
			else
				return EntryPoint.None;
		}

		enum EntryPoint
		{
			None = 0,
			North = 1,
			South = -1,
			East = 2,
			West = -2
		}
	}
}
