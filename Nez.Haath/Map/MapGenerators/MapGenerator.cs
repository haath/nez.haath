﻿
using System;
using System.Linq;
using System.Collections.Generic;

using Nez;
using Nez.Tiled;


using Microsoft.Xna.Framework;

namespace Nez.Haath.Map
{
	public abstract class MapGenerator
	{
		protected List<TiledMap> tiledMaps;
		protected TiledMapDescription mapDescription;

		protected TiledMapEntity mainEntity;

		public MapGenerator(List<TiledMap> tiledMaps, TiledMapDescription mapDescription)
		{
			this.tiledMaps = tiledMaps;
			this.mapDescription = mapDescription;
		}

		public abstract TiledMapEntity Generate();
		
		public virtual Vector2 PlayerSpawnPoint()
		{
			return mainEntity.GetObjectWithName("player").position;
		}
	}
}
