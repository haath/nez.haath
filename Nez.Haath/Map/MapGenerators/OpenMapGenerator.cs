﻿using System;
using System.Linq;
using System.Collections.Generic;

using Nez;
using Nez.Tiled;

using Microsoft.Xna.Framework;

namespace Nez.Haath.Map
{
	public class OpenMapGenerator : MapGenerator
	{
		int GridTileWidth
		{
			get { return tiledMaps[0].tilesets[0].tileWidth; }
		}
		int GridTileHeight
		{
			get { return tiledMaps[0].tilesets[0].tileHeight; }
		}

		bool[,] grid;

		public OpenMapGenerator(List<TiledMap> tiledMaps, TiledMapDescription mapDescription, int width, int height) : base(tiledMaps, mapDescription)
		{
			grid = new bool[width, height];
		}

		public override TiledMapEntity Generate()
		{
			// Place the initial map at (0, 0)
			mainEntity = PlaceAt(PickRandomWithConstraint(Point.Zero), Point.Zero);

			for (int x = 0; x < grid.GetLength(0); x++)
			{
				for (int y = 0; y < grid.GetLength(1); y++)
				{
					if (!grid[x, y])
					{
						// This spot in the grid is not already occupied.
						TiledMap map = PickRandomWithConstraint(ConstraintForSlot(x, y));
						if (map != null)
						{
							PlaceAt(map, new Point(x, y));
						}
					}
				}
			}

			return mainEntity;
		}

		Point ConstraintForSlot(int x, int y)
		{
			Point constraints = Point.Zero;

			while (x < grid.GetLength(0) && grid[x++, y])
				constraints.X++;

			x--;

			while (y < grid.GetLength(1) && grid[x, y++])
				constraints.Y++;

			return constraints;
		}

		TiledMap PickRandomWithConstraint(Point constaint)
		{
			List<TiledMap> options = tiledMaps.Where(map => 
				(constaint.X == 0 || MapSpaceInGrid(map).X <= constaint.X)
				&& (constaint.Y == 0 || MapSpaceInGrid(map).Y <= constaint.Y)
			).ToList();
			return options.randomItem();
		}

		TiledMapEntity PlaceAt(TiledMap tiledMap, Point position)
		{
			TiledMapEntity mapEntity = new TiledMapEntity(tiledMap, mapDescription);

			if (mainEntity != null)
			{
				mainEntity.AppendMap(mapEntity, new Point(position.X * GridTileWidth, position.Y * GridTileHeight));
			}

			for (int x = 0; x < MapSpaceInGrid(tiledMap).X; x++)
			{
				for (int y = 0; y < MapSpaceInGrid(tiledMap).Y; y++)
				{
					grid[x, y] = true;
				}
			}

			return mapEntity;
		}

		Point MapSpaceInGrid(TiledMap map)
		{
			return new Point(
				(int)Math.Ceiling(map.width / GridTileWidth * 1.0),
				(int)Math.Ceiling(map.height / GridTileHeight * 1.0)
				);
		}
	}
}
