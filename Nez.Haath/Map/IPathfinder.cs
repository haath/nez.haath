﻿
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Nez.Haath
{
	public interface IPathfinder
	{
		List<Vector2> Pathfind(Vector2 start, Vector2 end);
	}
}
