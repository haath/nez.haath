﻿using Nez;


namespace Nez.Haath.Map
{
	/// <summary>
	/// Holds the description of a layer.
	/// </summary>
	public struct TiledLayerDescription
	{
		/// <summary>
		/// The name of the layer, as it is set in the .tmx file
		/// </summary>
		public readonly string LayerName;

		/// <summary>
		/// The order in which this layer will be rendered on the screen
		/// </summary>
		public readonly RenderLayer RenderLayer;

		/// <summary>
		/// The physicsLayer the map will be placed on.
		/// </summary>
		public readonly CollisionLayers CollisionLayers;

		/// <summary>
		/// Custom material.
		/// Use Material.stencilWrite() and loadNezEffect(SpriteAlphaTestEffect)
		/// </summary>
		public readonly Material LayerMaterial;


		public TiledLayerDescription(string layerName, RenderLayer renderLayer, CollisionLayers collisionLayers, Material layerMaterial)
		{
			LayerName = layerName;
			RenderLayer = renderLayer;
			CollisionLayers = collisionLayers;
			LayerMaterial = layerMaterial;
		}

		public TiledLayerDescription(string layerName, RenderLayer renderLayer, CollisionLayers collisionLayers)
			: this(layerName, renderLayer, collisionLayers, null) { }
	}
}
