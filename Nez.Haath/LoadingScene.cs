﻿using Nez.Sprites;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using Nez;
using System;
using Nez.Tweens;

namespace Nez.Haath
{ 
	public class LoadingScene<T> : LoadingRenderScreen where T : Scene, new()
	{
		int designWidth;
		int designHeight;


		public LoadingScene(int designWidth, int designHeight) : base()
		{
			this.designWidth = designWidth;
			this.designHeight = designHeight;
		}

		public override void initialize()
		{
			base.initialize();

			// default to 1280x720 with no SceneResolutionPolicy
			setDesignResolution(designWidth, designHeight, Scene.SceneResolutionPolicy.None );
			Screen.setSize(designWidth, designHeight);

			TweenManager.stopAllTweens();
			FadeTransition transition = new FadeTransition(() => Activator.CreateInstance(typeof(T)) as Scene);
			transition.fadeInDuration = 0.2f;
			transition.fadeOutDuration = 0.0f;

			Core.startSceneTransition(transition);
		}
	}

	public abstract class LoadingRenderScreen : Scene, IFinalRenderDelegate
	{
		public const int SCREEN_SPACE_RENDER_LAYER = 999;

		ScreenSpaceRenderer _screenSpaceRenderer;


		public LoadingRenderScreen(bool addExcludeRenderer = true, bool needsFullRenderSizeForUI = false)
		{
			// setup one renderer in screen space for the UI and then (optionally) another renderer to render everything else
			if (needsFullRenderSizeForUI)
			{
				// dont actually add the renderer since we will manually call it later
				_screenSpaceRenderer = new ScreenSpaceRenderer(100, SCREEN_SPACE_RENDER_LAYER);
				_screenSpaceRenderer.shouldDebugRender = false;
				finalRenderDelegate = this;
			}
			else
			{
				addRenderer(new ScreenSpaceRenderer(100, SCREEN_SPACE_RENDER_LAYER));
			}

			if (addExcludeRenderer)
				addRenderer(new RenderLayerExcludeRenderer(0, SCREEN_SPACE_RENDER_LAYER));
		}


		#region IFinalRenderDelegate

		public Scene scene { get; set; }

		public void onAddedToScene()
		{ }


		public void onSceneBackBufferSizeChanged(int newWidth, int newHeight)
		{
			_screenSpaceRenderer.onSceneBackBufferSizeChanged(newWidth, newHeight);
		}


		public void handleFinalRender(Color letterboxColor, RenderTarget2D source, Rectangle finalRenderDestinationRect, SamplerState samplerState)
		{
			Core.graphicsDevice.SetRenderTarget(null);
			Core.graphicsDevice.Clear(letterboxColor);
			Graphics.instance.batcher.begin(BlendState.Opaque, samplerState, DepthStencilState.None, RasterizerState.CullNone, null);
			Graphics.instance.batcher.draw(source, finalRenderDestinationRect, Color.White);
			Graphics.instance.batcher.end();

			_screenSpaceRenderer.render(scene);
		}

		#endregion

	}
}

