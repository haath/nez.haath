﻿
using Nez;

namespace Nez.Haath
{
	/// <summary>
	/// A simple implementation of a boolean switch.
	/// <para>Useful when flags need to be referenced from multiple places while also switching values for certain amounts of time.</para>
	/// <para>The switch can then be used as a boolean field where it will return true only in the ON state.</para>
	/// </summary>
	public class Switch
	{
		bool val;
		bool timerRunning;

		/// <summary>
		/// Initializes a new Switch in a pre-defined position.
		/// </summary>
		/// <param name="on">If true, the Switch will begin in the ON state.</param>
		public Switch(bool on)
		{
			val = on;
			timerRunning = false;
		}

		/// <summary>
		/// Initializes a new Switch in the OFF state.
		/// </summary>
		public Switch() : this(false) { }

		/// <summary>
		/// Sets the Switch to the ON state.
		/// </summary>
		public void TurnOn()
		{
			val = true;
		}

		/// <summary>
		/// Sets the switch to the OFF state.
		/// </summary>
		public void TurnOff()
		{
			val = false;
		}

		/// <summary>
		/// Flips the switch from its current state to the other.
		/// </summary>
		public void Flip()
		{
			val = !val;
		}

		/// <summary>
		/// Flips the switch from its current state to the other for a given duration and then flips it back.
		/// </summary>
		/// <param name="seconds">The duration in seconds.</param>
		public void FlipFor(float seconds)
		{
			if (timerRunning)
				throw new System.InvalidOperationException("There is a timer already running on this Switch.");

			bool curVal = val;

			val = !val;

			timerRunning = true;

			Core.schedule(seconds, t =>
			{
				timerRunning = false;
				val = curVal;
			});
		}

		/// <summary>
		/// Returns true if the switch is in the ON state.
		/// </summary>
		/// <param name="sw"></param>
		/// <returns></returns>
		public static bool operator true(Switch sw)
		{
			return sw.val;
		}

		/// <summary>
		/// Returns true if the switch is in the OFF state.
		/// </summary>
		/// <param name="sw"></param>
		/// <returns></returns>
		public static bool operator false(Switch sw)
		{
			return !sw.val;
		}

		public static bool operator !(Switch sw)
		{
			return sw.val == false;
		}

		public static bool operator &(Switch sw1, bool sw2)
		{
			return (sw1.val == true) && (sw2);
		}

		public static bool operator |(Switch sw1, bool sw2)
		{
			return (sw1.val == true) || (sw2);
		}

		public override string ToString()
		{
			return val.ToString();
		}
	}
}
