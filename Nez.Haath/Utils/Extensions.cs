﻿using System;
using System.Linq;
using System.Collections.Generic;

using Nez.Tiled;

using Microsoft.Xna.Framework;

namespace Nez.Haath.Extensions
{

	public static class Extensions
	{
		/// <summary>
		/// Returns a random vector to one of the eight directions.
		/// </summary>
		/// <returns></returns>
		public static Vector2 NextDirection()
		{
			Vector2 dir = Vector2.Zero;

			// Re-roll if we get zero.
			while ((dir = new Vector2(Nez.Random.minusOneToOne(), Nez.Random.minusOneToOne())) == Vector2.Zero) ;

			return dir.Normalized();
		}

		/// <summary>
		/// Returns the direction of the vector
		/// </summary>
		/// <param name="vector"></param>
		/// <returns></returns>
		public static Vector2 Normalized(this Vector2 vector)
		{
			return vector.Length() != 0 ? vector / vector.Length() : Vector2.Zero;
		}

		/// <summary>
		/// Clamps a vector to its nearest direction.
		/// </summary>
		/// <param name="vector"></param>
		/// <returns>A unit vector of the direction.</returns>
		public static Vector2 ToFourWayDirection(this Vector2 vector)
		{
			if (Math.Abs(vector.X) >= Math.Abs(vector.Y) && Math.Abs(vector.X) > Mathf.epsilon)
			{
				if (vector.X > 0)
				{
					return new Vector2(1, 0);
				}
				else
				{
					return new Vector2(-1, 0);
				}
			}
			else if (Math.Abs(vector.Y) > Math.Abs(vector.X) && Math.Abs(vector.Y) > Mathf.epsilon)
			{
				if (vector.Y > 0)
				{
					return new Vector2(0, 1);
				}
				else
				{
					return new Vector2(0, -1);
				}
			}
			else
			{
				return Vector2.Zero;
			}
		}

		/// <summary>
		/// Calculates the angle in radians between two Vector2.
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		public static float AngleTo(this Vector2 v1, Vector2 v2)
		{
			return (float)Math.Atan2(v2.Y - v1.Y, v2.X - v1.X);
		}

		/// <summary>
		/// Returns any trigger colliders that overlap with the given collider.
		/// </summary>
		/// <param name="collider"></param>
		/// <returns></returns>
		public static List<CollisionResult> TriggersWith(this Collider collider)
		{
			List<CollisionResult> result = new List<CollisionResult>();

			IEnumerable<Collider> others = Physics.boxcastBroadphaseExcludingSelf(collider, collider.collidesWithLayers);

			foreach (Collider other in others)
			{
				CollisionResult res;
				if (other.isTrigger && collider.collidesWith(other, out res))
					result.Add(res);
			}

			return result;
		}

		public static TiledObject Clone(this TiledObject obj)
		{
			return new TiledObject()
			{
				height = obj.height,
				id = obj.id,
				name = obj.name,
				objectType = obj.objectType,
				polyPoints = obj.polyPoints.ToArray(),
				position = obj.position,
				properties = obj.properties.ToDictionary(x => x.Key, x => x.Value),
				rotation = obj.rotation,
				tiledObjectType = obj.tiledObjectType,
				type = obj.type,
				visible = obj.visible,
				width = obj.width,
				x = obj.x,
				y = obj.y
			};
		}

		public static bool IsUp(this Vector2 vector)
		{
			return vector.X == 0 && vector.Y < 0;
		}
		public static bool IsDown(this Vector2 vector)
		{
			return vector.X == 0 && vector.Y > 0;
		}
		public static bool IsLeft(this Vector2 vector)
		{
			return vector.X < 0 && vector.Y == 0;
		}
		public static bool IsRight(this Vector2 vector)
		{
			return vector.X > 0 && vector.Y == 0;
		}
	}
}
