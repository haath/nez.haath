﻿namespace Nez.Haath
{
	using System;

	public enum ColliderType
	{
		Circle,
		Box
	}

	/// <summary>
	/// Layers of the scene, rendeing visible in the following order
	/// <para>Top > Interface > Weather > Characters > Ground > Bottom</para>
	/// </summary>
	public enum RenderLayer
	{
		/// <summary>
		/// This layer will not be rendered
		/// Useful when having the collision data in a separate layer
		/// </summary>
		Hidden = 100,

		/// <summary>
		/// Rendered behind everything in the scene
		/// </summary>
		Bottom = 99,

		/// <summary>
		/// Rendered below the ground
		/// </summary>
		BelowGround = 11,

		/// <summary>
		/// Ground layer
		/// </summary>
		Ground = 10,

		/// <summary>
		/// Layer above the ground for details like plants and trees
		/// </summary>
		AboveGround = 9,

		/// <summary>
		/// Layer below the characters but above the ground
		/// </summary>
		BelowCharacters = 2,

		/// <summary>
		/// Placew NPCs here so they don't ever hide the player
		/// </summary>
		NonPlayerCharacters = 1,

		/// <summary>
		/// Layer for placing players and NPCs
		/// </summary>
		Characters = 0,

		/// <summary>
		/// Layer for placing projectiles
		/// </summary>
		Projectiles = -2,

		/// <summary>
		/// Layer for details that should be seen above the characters
		/// </summary>
		AboveCharacters = -3,

		Light = 95,

		/// <summary>
		/// Layer for details visible under the weather
		/// </summary>
		BelowWeather = -10,

		/// <summary>
		/// Layer for weather effects
		/// </summary>
		Weather = -15,

		/// <summary>
		/// Details visible above the weather
		/// </summary>
		AboveWeather = -16,

		/// <summary>
		/// Under the standard UI layer
		/// </summary>
		BelowInterface = -19,

		/// <summary>
		/// The UI layer
		/// </summary>
		Interface = -20,

		/// <summary>
		/// Above the UI layer
		/// </summary>
		AboveInterface = -21,

		/// <summary>
		/// Rendered above everything, including the UI
		/// </summary>
		Top = -99
	}

	/// <summary>
	/// Layers to seperate different kinds of collisions between various objects.
	/// </summary>
	[Flags]
	public enum CollisionLayers
	{
		/// <summary>
		/// Will collide with nothing.
		/// </summary>
		None		= 0,

		/// <summary>
		/// Will collide with ground-based obstacles like rocks and water.
		/// <para>For example, flying enemies or projectiles could ignore this.</para>
		/// </summary>
		Ground		= 1 << 0,

		/// <summary>
		/// Will collide with walls.
		/// </summary>
		Walls		= 1 << 1,

		/// <summary>
		/// Will collide with projectiles. Useful for trigger-collider projectile detectors.
		/// </summary>
		Projectiles	= 1 << 2,

		/// <summary>
		/// This is not implemented anywhere by the Engine but can be used for characters to collid with each other without being set as a ground obstacle.
		/// </summary>
		Characters	= 1 << 3,

		/// <summary>
		/// Objects on this layer will cast a shadow.
		/// </summary>
		Shadows		= 1 << 4
	}
}
