
# RPG Maker Characters

The `RmCharacter` class is an entity that makes up a character from an [RPG Maker styled sprite](https://www.google.com/search?q=rpg+maker+sprite&client=firefox-b-ab&tbm=isch&tbo=u&source=univ&sa=X&ved=2ahUKEwirmo-eg9ndAhWsiqYKHUNiAPwQsAR6BAgFEAE&biw=1920&bih=954).

```csharp
Texture2D sprite;

RmCharacter character = new RmCharacter(sprite);

scene.addEntity(character);
```

Alternatively you may want to extend this class to create your own entities.

```csharp
class Player : RmCharacter
{
    public Player(Texture2D sprite) : base(sprite)
    {
        // ...
    }
}
```