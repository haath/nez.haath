
# Installation

Add the following to your NuGet sources.

```url
https://nuget.gmantaos.com/
```

Add the following packages.

- [Nez.Haath](https://nuget.gmantaos.com/?specialType=singlePackage&id=Nez.Haath)
- [Nez.PipelineImporter](https://nuget.gmantaos.com/?specialType=singlePackage&id=Nez.PipelineImporter)
- [MonoGame.Framework.DesktopGL](https://www.nuget.org/packages/MonoGame.Framework.DesktopGL/3.7.0.1681-develop)

Add the following to the project's `Content.mgcb`.

```bash
#----------------------------- Global Properties ---------------------------#

...

#-------------------------------- References -------------------------------#

/reference:../../packages/Nez.PipelineImporter.0.9.2/tools/Nez.dll
/reference:../../packages/Nez.PipelineImporter.0.9.2/tools/Nez.PipelineImporter.dll
/reference:../../packages/Nez.PipelineImporter.0.9.2/tools/Newtonsoft.Json.dll
/reference:../../packages/Nez.PipelineImporter.0.9.2/tools/Ionic.ZLib.dll

#---------------------------------- Content --------------------------------#

...
```