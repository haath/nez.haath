# Tiled Maps

This framework offers an abstraction over describing the layers of individual Tiled maps.

```csharp
TiledMapDescription description = new TiledMapDescription()
{
    // Layer name           Render layer                Collsion layer         
    { "ground",             RenderLayer.Ground                                  },
    { "fringe",             RenderLayer.AboveGround                             },
    { "above_characters",   RenderLayer.AboveCharacters                         },
    { "walls",              RenderLayer.Hidden,         CollisionLayers.Walls   },
    { "collision",          RenderLayer.Hidden,         CollisionLayers.Ground  }
};
```

It also supports a method of stitching multiple Tiled maps together, to create larger ones. This way, maps can be modular, updated dynamically, while also enabling pathfinding to work through them.

```csharp
TiledMap northMap = content.Load<TiledMap>("maps/north");
TiledMap sotuhMap = content.Load<TiledMap>("maps/south");
TiledMap eastMap = content.Load<TiledMap>("maps/east");

TiledMapEntity north = new TiledMapEntity(northMap, description);
TiledMapEntity south = new TiledMapEntity(sotuhMap, description);
TiledMapEntity east = new TiledMapEntity(eastMap, description);

// Assuming that the north tilemap is 32 tiles in height,
// this should put the south one directly below it.
Point southPoint = new Point(0, 32);

// Similarly for the map to the east.
Point eastPoint = new Point(32, 0);

north.AppendMap(south, southPoint);
north.AppendMap(east, eastPoint);

addEntity(north);
```