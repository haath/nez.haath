
<p align="center">
  <img src="img/logo.png"/>
  <h3 align="center">Naz.Haath</h3>
  <p align="center">An extension/wrapper of the Nez framework for Monogame, providing reusable classes for games based on Tiled maps.</p>
</p>
---

![](https://git.gmantaos.com/gamedev/Nez.Haath/badges/master/pipeline.svg)
[![Join the chat at https://gitter.im/Nez-Devs/Lobby](https://badges.gitter.im/Nez-Devs/Lobby.svg)](https://gitter.im/Nez-Devs/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

