
# Top-Down Player Controller

The `TopDownPlayerController` class is a wrapper class, defining controls for top-down 2D movement.

```csharp
using Nez.Haath;

public class Player : Entity
{
    public Player()
    {
        TopDownPlayerController controller = addComponent<TopDownPlayerController>();
        controller.OnInteract		+= () => {  };
        controller.OnLook			+= (direction) => { };
        controller.OnAttack			+= () => { };
        controller.OnCancel			+= () => { };
        controller.OnHotbar         += (hotbarNumber) => { };

        controller.GamePadDeadzone  = 0.2f;
        controller.AttackInterval   = 0.2f;

        controller.MovementSpeed    = 250; // pixels per sec

        // The facing direction is required for the controller
        // to interact with Interactables.
        controller.FacingDirection  = () => { return new Vector2(x, y); };

        controller.OnInteractWith   += (interactable) => { };
    }
}
```