![Nez](Docs/img/logo.png)

[![](https://git.gmantaos.com/gamedev/Nez.Haath/badges/master/pipeline.svg)](https://git.gmantaos.com/gamedev/Nez.Haath/pipelines)
[![](https://img.shields.io/badge/docs-mkdocs-blue.svg?longCache=true)](https://gamedev.gmantaos.com/Nez.Haath/)
[![Join the chat at https://gitter.im/Nez-Devs/Lobby](https://badges.gitter.im/Nez-Devs/Lobby.svg)](https://gitter.im/Nez-Devs/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

----

